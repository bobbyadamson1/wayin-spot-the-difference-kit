import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';
import nodeResolve from 'rollup-plugin-node-resolve';

const bubleOpts = {
	transforms: {
		dangerousForOf: true,
		modules: false
	}
}

export default {
	entry: 'src/app.js',
	format: 'iife',
	moduleName: 'WYN_SPOTTHEDIFFERENCE',
	dest: 'dist/app.min.js',
	context: 'window',
	plugins: [
		nodeResolve({
	        jsnext: true,
	        main: true,
	        builtins: false,
	        browser: true,
	        extensions: ['.js', '.json']
	    }),
		buble(bubleOpts),
		uglify()
	]
}