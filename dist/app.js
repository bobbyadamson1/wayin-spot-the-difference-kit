(function () {
'use strict';

var ComparisonMap = function ComparisonMap(images, coordinates, options) {
    this.images = images;
    this.coordinates = coordinates;
    this.coordinatesFound = 0;
    this.returnCoordinates = options.returnCoordinates || false;
    this.returnCorrectAnswers = options.returnCoordinates || true;
    this.debug = options.debug === true;

    // Add event listeners
    this.initialize.call(this);
};

ComparisonMap.prototype.initialize = function initialize () {
    document.addEventListener('click', function(e) {
            var this$1 = this;

        var clickedImage = this.images.filter(function (Image) { return !Image.ignoreClicks && Image.proxy === e.target; });
        // Only want to run if an image was clicked
        if(clickedImage.length) {
            var 
                relativeClickedCoordinates = this.getClickLocationRelativeToEl(this.getClickLocation(e), clickedImage[0].selector),
                coordinateWasClicked = this.coordinatesClicked(relativeClickedCoordinates);

            if(coordinateWasClicked) {
                this.debug && console.log('WYN_SPOTTHEDIFFERENCE: Coordinate clicked: ', coordinateWasClicked);

                for(var coordinate in coordinateWasClicked) {
                    var 
                        coordinateLocationInMap = parseInt(coordinateWasClicked[coordinate]),
                        coordinateObject = this$1.coordinates[coordinateLocationInMap];

                    if(!coordinateObject.found) {
                        coordinateObject.findCoordinate();
                            
                        for(var image in this$1.images) {
                            var imageObject = this$1.images[image];

                            imageObject.renderDifferences && imageObject.renderDifference(coordinateObject);
                        }
                    }
                }
            }
        }
    }.bind(this), false);
};

ComparisonMap.prototype.getCoordinateNumber = function getCoordinateNumber () {
    return this.coordinates.length;
};

ComparisonMap.prototype.getCorrectAnswers = function getCorrectAnswers () {
    return this.returnCorrectAnswers ? this.coordinatesFound : false;
};

ComparisonMap.prototype.getClickLocation = function getClickLocation (e) {
    return {
        posx: e.pageX,
        posy: e.pageY
    }
};

ComparisonMap.prototype.getBoundsOfEl = function getBoundsOfEl (el) {
    var boundingRect = el.getBoundingClientRect();

    return {
        top: boundingRect.top + window.scrollY,
        right: boundingRect.right + window.scrollX,
        bottom: boundingRect.bottom + window.scrollY,
        left: boundingRect.left + window.scrollX,
        width: boundingRect.width,
        height: boundingRect.height,
    }
};

ComparisonMap.prototype.getClickLocationRelativeToEl = function getClickLocationRelativeToEl (clickCoordinates, el) {
    var elBounds = this.getBoundsOfEl(el);

    this.debug && console.log('WYN_SPOTTHEDIFFERENCE: Checking click location: click coordinates -', clickCoordinates, 'Image bounds: ', elBounds);

    if(clickCoordinates.posx < elBounds.left 
        || clickCoordinates.posx > elBounds.right 
        || clickCoordinates.posy < elBounds.top 
        || clickCoordinates.posy > elBounds.bottom ) {
        return false;
    } else {
        return {
            posxPercentage: (clickCoordinates.posx - elBounds.left) / elBounds.width * 100,
            posyPercentage: (clickCoordinates.posy - elBounds.top) / elBounds.height * 100
        }
    }
};

ComparisonMap.prototype.coordinatesClicked = function coordinatesClicked (relativeClickCoordinates) {
    var clickx = relativeClickCoordinates.posxPercentage;
        var clicky = relativeClickCoordinates.posyPercentage;
        var mapCoordinates = this.coordinates;
    var coordinatesFound = [];

    for(var coordinate in mapCoordinates) {
        if(clickx > mapCoordinates[coordinate].x 
            && clickx < mapCoordinates[coordinate].x + mapCoordinates[coordinate].w
            && clicky > mapCoordinates[coordinate].y
            && clicky < mapCoordinates[coordinate].y + mapCoordinates[coordinate].h) {
                coordinatesFound.push(coordinate);
        }
    }

    return coordinatesFound.length ? coordinatesFound : false;
};

var Coordinate = function Coordinate(props) {
    this.found = false;
    this.x = props.x;
    this.y = props.y;
    this.w = props.w;
    this.h = props.h;
    this.highlightClasses = props.classList || [];
};

Coordinate.prototype.findCoordinate = function findCoordinate () {
    this.found = true;
    return this;
};

Coordinate.prototype.toggleFound = function toggleFound () {
    this.found = !this.found;
    return this;
};

Coordinate.prototype.findMe = function findMe () {
        
};

var Image = function Image(props) {
    this.container = props && props.container ? props.container : null;
    this.selector = props && props.container ? props.container.querySelector('img') : null;
    this.ignoreClicks = props.ignoreClicks === true;
    this.renderDifferences = props.renderDifferences !== false;

    // We need the difference highlighter elements to obey positioning rules
    this.initialize();
};

Image.prototype.initialize = function initialize () {
    this.container.style.position = "relative";
    this.container.style.display = "inline-block";
    this.proxy = this.createProxy();
    this.container.appendChild(this.proxy);

    return this;
};

Image.prototype.createProxy = function createProxy () {
    var 
        proxy = document.createElement('div'),
        proxyStyles = proxy.style;

    proxyStyles.position = "absolute";
    proxyStyles.top = proxyStyles.right = proxyStyles.bottom = proxyStyles.left = 0;
    proxyStyles.display = "block";
    proxyStyles.zIndex = 9999;
    proxy.classList.add('imageProxy');

    return proxy;
};

Image.prototype.renderDifference = function renderDifference (elementProperties) {
    if(this.renderDifferences) {
        var x = elementProperties.x;
            var y = elementProperties.y;
            var w = elementProperties.w;
            var h = elementProperties.h;
            var highlightClasses = elementProperties.highlightClasses;

        var 
            differenceHighlighter = document.createElement('div'),
            differenceStyles = differenceHighlighter.style;

        differenceStyles.position = "absolute";
        differenceStyles.display  = "block";
        differenceStyles.left = x + "%";
        differenceStyles.top  = y + "%";
        differenceStyles.width= w + "%";
        differenceStyles.height   = h + "%";
        (ref = differenceHighlighter.classList).add.apply(ref, [ 'differenceHighlighter' ].concat( highlightClasses ));

        this.container.appendChild(differenceHighlighter);
        return true;
    } else {
        return false;
    }
        var ref;
};

var images = window.images = [
    new Image({
        container: document.querySelector('#question_01Wrapper .xFieldRadioChoice:first-child .xMediaContainer'), 
        ignoreClicks: true, 
        renderDifferences: false
    }),
    new Image({container: document.querySelector('#question_01Wrapper .xFieldRadioChoice:last-child .xMediaContainer')}) ];

var coordinates = window.coordinates = [
    new Coordinate({x: 20, y: 20, w: 25, h: 40}),
    new Coordinate({x: 0, y: 0, w: 50, h: 60}),
    new Coordinate({x: 80, y: 60, w: 20, h: 25}) ];

var ComparisonMap1 = window.c = new ComparisonMap(images, coordinates, {
    returnCoordinates: true
});

}());
