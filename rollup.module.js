import buble from 'rollup-plugin-buble';
import nodeResolve from 'rollup-plugin-node-resolve';

const bubleOpts = {
	transforms: {
		dangerousForOf: true,
		modules: false
	}
}

export default {
	entry: 'src/module.js',
	format: 'es',
	dest: 'dist/module.js',
	context: 'window',
	plugins: [ 
		nodeResolve({
	        jsnext: true,
	        main: true,
	        builtins: false,
	        browser: false,
	        extensions: ['.js', '.json']
	    }),
		buble(bubleOpts)
	]
}