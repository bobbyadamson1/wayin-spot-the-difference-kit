import {assert} from 'chai';
import {JSDOM} from 'jsdom';
import Image from '../src/classes/Image';

// Fake DOM
const 
    dom = new JSDOM(`<!DOCTYPE html><div class="image-container"><img src="https://via.placeholder.com/350x150" /></div>`),
    document = dom.window.document,
    domEls = {
        container: document.querySelector('.image-container'),
        image: document.querySelector('img'),
    };
    
// Mock images
const
    image_without_selector = new Image({}),
    image_valid = new Image({container: domEls.container});

    console.log(image_without_selector)
// Tests
describe('Image factory', function() {
    it('Should return false if not passed selector to an image', function() {
        assert.isFalse(image_without_selector);
    });

    it('Should return an object if passed a selector', function() {
        assert.isObject(image_valid);
    });
})