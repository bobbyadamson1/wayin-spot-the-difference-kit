import {assert} from 'chai';
import {JSDOM} from 'jsdom';
import Coordinate from '../src/classes/Coordinate';

const
    coordinate_valid = new Coordinate({x: 20, y: 20, w: 25, h: 30});

// Tests
describe('Coordinate factory', function() {
    it('Should return location x percentage value');
    it('Should return location y percentage value');
    it('Should return location width percentage value');
    it('Should return location height percentage value');
    it('Should not be found yet');
});