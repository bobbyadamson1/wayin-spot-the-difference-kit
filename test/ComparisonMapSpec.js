import {assert} from 'chai';

describe('Comparison Map factory', function() {
    describe('#registerImage()', function() {
        it('Should take an object which is an instance of the Image class');
        it('Should return an error if the object is not an instance of Image class');
        it('Should add Image instance to its "Images" array');
    });

    describe('#registerCoordinates()', function() {
        it('Should return an error if parameter is not an instance of the Coordinate class');
        it('Should return an error if the object is not an instance of the Coordinate class');
        it('Should add Coordinate to the Map instance "Coordinates" array');
    });

    describe('#checkLocation', function() {
        it('Should return an error if parameter does not have an X prop');
        it('Should return an error if parameter does not have an X prop value of type float');
        it('Should return an error if parameter does not have an Y prop');
        it('Should return an error if parameter does not have an Y prop value of type float');
        it('Should return true if the location matches one that is contained within a valid coordinate');
    });
})