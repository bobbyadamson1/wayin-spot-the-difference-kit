class ComparisonMap {
    constructor(images, coordinates, options) {
        this.images = images;
        this.coordinates = coordinates;
        this.coordinatesFound = 0;
        this.returnCoordinates = options.returnCoordinates || false;
        this.returnCorrectAnswers = options.returnCoordinates || true;
        this.debug = options.debug === true;

        // Add event listeners
        this.initialize.call(this);
    }

    initialize() {
        document.addEventListener('click', function(e) {
            const clickedImage = this.images.filter(Image => !Image.ignoreClicks && Image.proxy === e.target);
            // Only want to run if an image was clicked
            if(clickedImage.length) {
                const 
                    relativeClickedCoordinates = this.getClickLocationRelativeToEl(this.getClickLocation(e), clickedImage[0].selector),
                    coordinateWasClicked = this.coordinatesClicked(relativeClickedCoordinates);

                if(coordinateWasClicked) {
                    this.debug && console.log('WYN_SPOTTHEDIFFERENCE: Coordinate clicked: ', coordinateWasClicked);

                    for(var coordinate in coordinateWasClicked) {
                        const 
                            coordinateLocationInMap = parseInt(coordinateWasClicked[coordinate]),
                            coordinateObject = this.coordinates[coordinateLocationInMap];

                        if(!coordinateObject.found) {
                            coordinateObject.findCoordinate();
                            
                            for(var image in this.images) {
                                const imageObject = this.images[image];

                                imageObject.renderDifferences && imageObject.renderDifference(coordinateObject);
                            }
                        }
                    }
                }
            }
        }.bind(this), false);
    }

    getCoordinateNumber() {
        return this.coordinates.length;
    }

    getCorrectAnswers() {
        return this.returnCorrectAnswers ? this.coordinatesFound : false;
    }

    getClickLocation(e) {
        return {
            posx: e.pageX,
            posy: e.pageY
        }
    }

    getBoundsOfEl(el) {
        const boundingRect = el.getBoundingClientRect();

        return {
            top: boundingRect.top + window.scrollY,
            right: boundingRect.right + window.scrollX,
            bottom: boundingRect.bottom + window.scrollY,
            left: boundingRect.left + window.scrollX,
            width: boundingRect.width,
            height: boundingRect.height,
        }
    }

    getClickLocationRelativeToEl(clickCoordinates, el) {
        const elBounds = this.getBoundsOfEl(el);

        this.debug && console.log('WYN_SPOTTHEDIFFERENCE: Checking click location: click coordinates -', clickCoordinates, 'Image bounds: ', elBounds);

        if(clickCoordinates.posx < elBounds.left 
            || clickCoordinates.posx > elBounds.right 
            || clickCoordinates.posy < elBounds.top 
            || clickCoordinates.posy > elBounds.bottom ) {
            return false;
        } else {
            return {
                posxPercentage: (clickCoordinates.posx - elBounds.left) / elBounds.width * 100,
                posyPercentage: (clickCoordinates.posy - elBounds.top) / elBounds.height * 100
            }
        }
    }

    coordinatesClicked(relativeClickCoordinates) {
        const 
            {posxPercentage: clickx, posyPercentage: clicky} = relativeClickCoordinates,
            mapCoordinates = this.coordinates;
        let coordinatesFound = [];

        for(var coordinate in mapCoordinates) {
            if(clickx > mapCoordinates[coordinate].x 
                && clickx < mapCoordinates[coordinate].x + mapCoordinates[coordinate].w
                && clicky > mapCoordinates[coordinate].y
                && clicky < mapCoordinates[coordinate].y + mapCoordinates[coordinate].h) {
                    coordinatesFound.push(coordinate);
            }
        }

        return coordinatesFound.length ? coordinatesFound : false;
    }
    // increment
};

export default ComparisonMap;