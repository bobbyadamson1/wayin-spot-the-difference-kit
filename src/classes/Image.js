class Image {
    constructor(props) {
        this.container = props && props.container ? props.container : null;
        this.selector = props && props.container ? props.container.querySelector('img') : null;
        this.ignoreClicks = props.ignoreClicks === true;
        this.renderDifferences = props.renderDifferences !== false;

        // We need the difference highlighter elements to obey positioning rules
        this.initialize();
    }

    initialize() {
        this.container.style.position = `relative`;
        this.container.style.display = `inline-block`;
        this.proxy = this.createProxy();
        this.container.appendChild(this.proxy);

        return this;
    }

    createProxy() {
        let 
            proxy = document.createElement('div'),
            proxyStyles = proxy.style;

        proxyStyles.position = `absolute`;
        proxyStyles.top = proxyStyles.right = proxyStyles.bottom = proxyStyles.left = 0;
        proxyStyles.display = `block`;
        proxyStyles.zIndex = 9999;
        proxy.classList.add('imageProxy');

        return proxy;
    }

    renderDifference(elementProperties) {
        if(this.renderDifferences) {
            const {x, y, w, h, highlightClasses} = elementProperties;

            let 
                differenceHighlighter = document.createElement('div'),
                differenceStyles = differenceHighlighter.style;

            differenceStyles.position = `absolute`;
            differenceStyles.display  = `block`;
            differenceStyles.left     = `${x}%`;
            differenceStyles.top      = `${y}%`;
            differenceStyles.width    = `${w}%`;
            differenceStyles.height   = `${h}%`;
            differenceHighlighter.classList.add('differenceHighlighter', ...highlightClasses);

            this.container.appendChild(differenceHighlighter);
            return true;
        } else {
            return false;
        }
    }
};

export default Image;