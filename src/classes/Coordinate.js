class Coordinate {
    constructor(props) {
        this.found = false;
        this.x = props.x;
        this.y = props.y;
        this.w = props.w;
        this.h = props.h;
        this.highlightClasses = props.classList || [];
    }

    findCoordinate() {
        this.found = true;
        return this;
    }

    toggleFound() {
        this.found = !this.found;
        return this;
    }

    findMe() {
        
    }
};

export default Coordinate;