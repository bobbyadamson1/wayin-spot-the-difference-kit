import ComparisonMap from './classes/ComparisonMap';
import Coordinate from './classes/Coordinate';
import Image from './classes/Image';

const images = window.images = [
    new Image({
        container: document.querySelector('#question_01Wrapper .xFieldRadioChoice:first-child .xMediaContainer'), 
        ignoreClicks: true, 
        renderDifferences: false
    }),
    new Image({container: document.querySelector('#question_01Wrapper .xFieldRadioChoice:last-child .xMediaContainer')}),
];

const coordinates = window.coordinates = [
    new Coordinate({x: 20, y: 20, w: 25, h: 40}),
    new Coordinate({x: 0, y: 0, w: 50, h: 60}),
    new Coordinate({x: 80, y: 60, w: 20, h: 25}),
];

let ComparisonMap1 = window.c = new ComparisonMap(images, coordinates, {
    returnCoordinates: true
});